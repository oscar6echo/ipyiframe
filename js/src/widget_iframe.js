import * as widgets from '@jupyter-widgets/base';
import { extend } from 'lodash';

import { version } from '../package.json';
import * as Utils from './widget_utils';

const semver_range = `~${version}`;

const IframeModel = widgets.DOMWidgetModel.extend(
    {
        defaults() {
            return extend(IframeModel.__super__.defaults.call(this), {
                _model_name: 'IframeModel',
                _view_name: 'IframeView',
                _model_module: 'ipyiframe',
                _view_module: 'ipyiframe',
                _model_module_version: semver_range,
                _view_module_version: semver_range,

                _id: 0,

                src: '',
                srcdoc: '',
                width: 400,
                height: 150,
                border: '',
                margin: '',
                padding: '',
                scrolling: '',
                scale: 1.0,
                transform_origin: 'top left',
                compress_level: 1,
            });
        },
    },
    {
        serializers: _.extend(widgets.DOMWidgetModel.serializers, {
            srcdoc: {
                deserialize: Utils.deserialize_content,
                serialize: Utils.serialize_content,
            },
        }),
    }
);

const IframeView = widgets.DOMWidgetView.extend({
    render() {
        this._id = this.model.get('_id');

        const src = this.model.get('src');
        const srcdoc = this.model.get('srcdoc');
        const width = this.model.get('width');
        const height = this.model.get('height');
        const border = this.model.get('border');
        const margin = this.model.get('margin');
        const padding = this.model.get('padding');
        const name = this.model.get('name');
        const scrolling = this.model.get('scrolling');
        const scale = this.model.get('scale');
        const transform_origin = this.model.get('transform_origin');

        const divWidget = this.el;

        const container = document.createElement('div');
        const widthContainer = Math.floor(width * scale) + 1;
        const heightContainer = Math.floor(height * scale) + 1 + 6;
        const styleContainer = `width: ${widthContainer}px;
                                height: ${heightContainer}px;
                                `;
        container.setAttribute('style', styleContainer);
        divWidget.appendChild(container);

        const iframe = document.createElement('iframe');

        iframe.id = `widget-iframe-${this._id}`;
        iframe.setAttribute('sandbox', 'allow-scripts');

        if (src !== '') {
            iframe.setAttribute('src', src);
        }
        if (srcdoc !== '') {
            iframe.setAttribute('srcdoc', srcdoc);
        }
        iframe.setAttribute('width', width);
        iframe.setAttribute('height', height);
        iframe.setAttribute('name', name);
        iframe.setAttribute('scrolling', scrolling);

        const style0 = `width: ${width}px;
                        height: ${height}px;
                        `;

        const style1 = `border: ${border};
                        margin: ${margin}; 
                        padding: ${padding};
                        `;
        const style2 = `-webkit-transform:scale(${scale});
                        -moz-transform:scale(${scale});
                        -o-transform:scale(${scale});
                        -ms-transform:scale(${scale});
                        transform:scale(${scale});

                        -webkit-transform-origin:${transform_origin};
                        -moz-transform-origin:${transform_origin};
                        -o-transform-origin:${transform_origin};
                        -ms-transform-origin:${transform_origin};
                        transform-origin: ${transform_origin};
                        `;
        iframe.setAttribute('style', style0 + style1 + style2);
        container.appendChild(iframe);

        console.log('widget Iframe ready');
    },
});

export { IframeModel, IframeView };
