import * as pako from 'pako';

function serialize_content(content, manager) {
    const enc = new TextEncoder();
    const level = manager.attributes.compress_level;

    const uintarray = enc.encode(content);
    const compressed = pako.deflate(uintarray, { level });
    const output = new DataView(compressed.buffer.slice(0));

    return output;
}

function deserialize_content(content) {
    const enc = new TextDecoder();

    const uintarray = pako.inflate(content.buffer);
    const output = enc.decode(uintarray);

    return output;
}

export { serialize_content, deserialize_content };
